" ____  ____  
"| __ )| __ ) 
"|  _ \|  _ \ 
"| |_) | |_) |
"|____/|____/ 
"             
" settings for neovim

call plug#begin('~/.vim/plugged')
" 	" Appearance plugins
" 	Plug 'vim-airline/vim-airline'
" 	Plug 'chriskempson/base16-vim'
" 	Plug 'gruvbox-community/gruvbox'
" 	Plug 'sonph/onehalf', {'rtp': 'vim/'}
" 	Plug 'vim-airline/vim-airline-themes'
" 	Plug 'junegunn/goyo.vim'
" 	Plug 'aonemd/kuroi.vim'
" 	" autocomplete and general language support
" 	Plug 'neoclide/coc.nvim', {'branch': 'release'}
" 	Plug 'lervag/vimtex'
" 	Plug 'dpelle/vim-LanguageTool'
    Plug 'matze/vim-tex-fold'
" 	" movement and integration plugins
" 	Plug 'tpope/vim-vinegar'
" 	Plug 'preservim/nerdtree'
" 	Plug 'voldikss/vim-floaterm'
" 	Plug 'junegunn/vim-peekaboo'
" 	Plug 'unblevable/quick-scope'
" 	Plug 'justinmk/vim-sneak'
" 	Plug 'airblade/vim-gitgutter'
" 	Plug 'junegunn/fzf.vim'
	Plug 'tpope/vim-commentary'
" 	Plug 'tpope/vim-fugitive'
"     Plug 'tpope/vim-surround'
" 	Plug 'jiangmiao/auto-pairs'
" 	Plug 'christoomey/vim-tmux-navigator'
" 	" markdown plugins
" 	Plug 'JamshedVesuna/vim-markdown-preview'
" 	Plug 'junegunn/vim-easy-align'
" 	Plug 'plasticboy/vim-markdown'
" 	Plug 'dhruvasagar/vim-marp'	
" 	Plug 'jkramer/vim-checkbox'
" 	Plug 'dkarter/bullets.vim'
" 	Plug 'chmp/mdnav'
call plug#end()

" Align GitHub-flavored Markdown tables
" au FileType markdown vmap <Leader><Bslash> :EasyAlign*<Bar><Enter>

" autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" nmap <C-u> :NERDTreeToggle<CR>
" let g:floaterm_keymap_toggle = '<A-t>'

" " ========================== "
" "     Vim appearance         "
" " ========================== "

" colorscheme gruvbox
" let g:airline_theme="gruvbox"
" let g:airline#extensions#tabline#enabled = 1
" let g:airline_powerline_fonts = 1

" syntax on
" filetype plugin indent on
" highlight Pmenu ctermbg=darkgray guibg=black
" hi Normal guibg=NONE ctermbg=NONE
" set guifont=DejaVu\ Sans\ Mono\ for\ Powerline:h14
" let g:pandoc#modules#disabled = ["folding"]

set autochdir
set clipboard+=unnamedplus " using system clipboard for vim
set nocompatible
set number relativenumber
set cursorline
set tabstop=4
set softtabstop=4   " number of spaces in tab when editing
set shiftwidth=4    " number of spaces to use for autoindent
set tabstop=4 shiftwidth=4 expandtab
" set scrolloff=25
set showmode
set shell=bash\ 
set spelllang=en
let g:colorizer_auto_color = 1
set mouse=a
" set nofoldenable    " disable folding
set conceallevel=0

"" ================================== "
""		settings for vimtex
"" ================================== "

"let g:tex_flavor='latex'
"let g:vimtex_view_metod='zathura'
"let g:vimtex_quickfix_mode=0
"" filetype on
"au BufRead,BufNewFile *.tex,*.sty,*.cls set filetype=tex
"au Filetype tex syntax region texZone start='\\begin{hcode}' end='\\end{hcode}'
"au Filetype tex syntax region texZone start='\\begin{code}' end='\\end{code}'

"" ================================== "
""		  settings for fzf
"" ================================== "

"let g:fzf_preview_window = ''
"map <A-.> :Files<CR>

"" ================================== "
"" settings for coc from their github
"" ================================== "

"source ~/.config/nvim/cocsettings.vim
"nmap <space>e :CocCommand explorer<CR>

"" ========================== "
""		 vim checkbox
"" ========================== "

"let g:checkbox_states = [" ", "x","o"]
"let g:insert_checkbox_prefix = "- "
"let g:insert_checkbox_suffix = " "

"" ========================== "
""    Remapped keybinds       "
"" ========================== "

"" Trigger a highlight only when pressing f and F.
"let g:qs_highlight_on_keys = ['f', 'F']

"" Split commands to easily move between splits see tmux config for integration
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"" moving between buffers
"" map <leader><leader> :bn <CR>
"" map <leader>a :bp <CR>

"" Reload config
"map <C-s> :source $HOME/.config/nvim/init.vim <CR>

"" building latex
autocmd FileType tex,bib nmap <buffer> <C-A> :w<bar>!latexmk ./main.tex -pdf -shell-escape %<CR>
autocmd FileType tex,bib nmap <buffer> <C-B> ! evince main.pdf &<CR>

"" compiling cpp 
autocmd FileType cpp nmap <buffer> <C-A> :w<bar>!g++ % -o %:r.exe <CR>

"" compiling c
autocmd FileType c nmap <buffer> <C-A> :w<bar>!gcc % -o %:r.exe <CR>

"" python autorun file
autocmd FileType python map <buffer> <C-A> :w<bar>!python3 % <CR>

"" markdown to pdf
autocmd FileType markdown map <buffer> <C-A> :w<bar>!pandoc % -o %:r.pdf <CR>

"" cytosim set autocomment
au BufRead,BufNewFile *.cym		setfiletype cym
autocmd FileType cym setlocal commentstring=%\ %s

"" ========================== "
""      custom commands       "
"" ========================== "

"" toggle background 
"command! Bgd :set bg=dark
"command! Bgl :set bg=light

" Workaround for gk/gj 
nmap j gj
nmap k gk
nnoremap gk :<C-u>call VSCodeCall('cursorMove', { 'to': 'up', 'by': 'wrappedLine', 'value': v:count ? v:count : 1 })<CR> 
nnoremap gj :<C-u>call VSCodeCall('cursorMove', { 'to': 'down', 'by': 'wrappedLine', 'value': v:count ? v:count : 1 })<CR> 
