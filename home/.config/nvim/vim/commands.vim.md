# Custom commands

## Toggle background

```vim
command! Bgd :set bg=dark
command! Bgl :set bg=light
```
