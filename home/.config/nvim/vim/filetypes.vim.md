# Filetype settings

## Cytosim

```vim
au BufRead,BufNewFile *.cym,*.tlp,*.tpl setfiletype cym
autocmd FileType cym setlocal commentstring=%\ %s
```

## Gnuplot

```vim
au BufRead,BufNewFile *.gp,*.plt setfiletype gp
autocmd FileType gp set syntax=gnuplot
autocmd FileType gp setlocal commentstring=#\ %s
```
