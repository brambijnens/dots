" Appearance settings
colorscheme gruvbox
let g:airline_theme = "gruvbox"
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 0

syntax on
filetype plugin indent on
highlight Pmenu ctermbg=darkgray guibg=black
hi Normal guibg=NONE ctermbg=NONE
set guifont=Hack:h14
let g:pandoc#modules#disabled = ["folding"]
set clipboard+=unnamedplus " using system clipboard for vim
set nocompatible
set number relativenumber
set cursorline
set tabstop=4
set softtabstop=4   " number of spaces in tab when editing
set shiftwidth=4    " number of spaces to use for autoindent
set tabstop=4 shiftwidth=4 expandtab
set scrolloff=25
set showmode
set shell=bash\ 
set spelllang=en
let g:colorizer_auto_color = 1
set mouse=a
set nofoldenable    " disable folding
set conceallevel=0
let g:python_style = 'numpy'

set hidden
set nobackup
set nowritebackup
set cmdheight=2
set updatetime=300
set shortmess+=c
set signcolumn=yes

