" ================================== "
"		settings for vimtex
" ================================== "

let g:tex_flavor='latex'
let g:vimtex_view_metod='zathura'
let g:vimtex_quickfix_mode=0
" filetype on
au BufRead,BufNewFile *.tex,*.sty,*.cls set filetype=tex
au Filetype tex syntax region texZone start='\\begin{hcode}' end='\\end{hcode}'
au Filetype tex syntax region texZone start='\\begin{code}' end='\\end{code}'
