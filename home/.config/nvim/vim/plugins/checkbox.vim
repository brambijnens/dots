" ========================== "
"		 vim checkbox
" ========================== "

let g:checkbox_states = [" ", "x","o"]
let g:insert_checkbox_prefix = "- "
let g:insert_checkbox_suffix = " "
