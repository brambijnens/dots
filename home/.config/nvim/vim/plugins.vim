" Plugins
call plug#begin('~/.vim/plugged')

"" Appearance plugins
    Plug 'vim-airline/vim-airline'
    Plug 'gruvbox-community/gruvbox'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'junegunn/goyo.vim'

"" Autocomplete and general language support
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'pixelneo/vim-python-docstring'
    Plug 'lervag/vimtex'

"" Movement and integration plugins
    Plug 'tpope/vim-vinegar'
    Plug 'preservim/nerdtree'
    Plug 'junegunn/vim-peekaboo'
    Plug 'unblevable/quick-scope'
    Plug 'justinmk/vim-sneak'
    Plug 'airblade/vim-gitgutter'
    Plug 'junegunn/fzf.vim'
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-surround'
    Plug 'jiangmiao/auto-pairs'
    Plug 'christoomey/vim-tmux-navigator'

"" Markdown plugins
    Plug 'junegunn/vim-easy-align'
    Plug 'plasticboy/vim-markdown'
    Plug 'jkramer/vim-checkbox'
    Plug 'dkarter/bullets.vim'
    Plug 'chmp/mdnav'
    Plug 'dhruvasagar/vim-table-mode' 
call plug#end()

