# Custom mappings

## Coc maps

Opening the file tree

```vim
nmap <C-u> :CocCommand explorer<CR>
```

Use `[g` and `]g` to navigate diagnostics

```vim
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)
```

GoTo code navigation.

```vim
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
```

Symbol renaming.

```vim
nmap <leader>rn <Plug>(coc-rename)
```

Formatting selected code.

```vim
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
```

Applying codeAction to the selected region.
Example: `<leader>aap` for current paragraph

```vim
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)
```

Remap keys for applying codeAction to the current line.

```vim
nmap <leader>ac  <Plug>(coc-codeaction)
```

Apply AutoFix to problem on the current line.

```vim
nmap qf  <Plug>(coc-fix-current)
```

Map function and class text objects
NOTE: Requires 'textDocument.documentSymbol' support from the language server.

```vim
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)
```

Use CTRL-S for selections ranges.
Requires 'textDocument/selectionRange' support of LS, ex: coc-tsserver

```vim
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)
```

Mappings using CoCList:
Show all diagnostics.

```vim
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
```

Split commands to easily move between splits see tmux config for integration

```vim
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
```

Navigating buffers using `Tab` and `Shift+Tab`

```vim
map <Tab> :bn <CR>
map <S-Tab> :bp <CR>
```

Reload config the neovim configuration

```vim
map <C-s> :source $HOME/.config/nvim/init.vim <CR>
```

Building latex documents using latexmk

```vim
autocmd FileType tex,bib nmap <buffer> <C-A> :w<bar>! latexmk ./main.tex -pdf -shell-escape <CR>
autocmd FileType tex,bib nmap <buffer> <C-B> ! evince main.pdf &<CR>
```

Compiling single cpp file

```vim
autocmd FileType cpp nmap <buffer> <C-A> :w<bar>!g++ % -o %:r.exe <CR>
```

Compiling single c file

```vim
autocmd FileType c nmap <buffer> <C-A> :w<bar>!gcc % -o %:r.exe <CR>
```

Run python file

```vim
autocmd FileType python map <buffer> <C-A> :w<bar>!python % <CR>
```

Convert markdown to pdf using pandoc

```vim
autocmd FileType markdown map <buffer> <C-A> :w<bar>!pandoc % -f commonmark_x -o %:r.pdf <CR>
```
