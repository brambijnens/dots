" Filetype settings
"" Cytosim
au BufRead,BufNewFile *.cym,*.tlp,*.tpl setfiletype cym
autocmd FileType cym setlocal commentstring=%\ %s

"" Gnuplot
au BufRead,BufNewFile *.gp,*.plt setfiletype gp
autocmd FileType gp set syntax=gnuplot
autocmd FileType gp setlocal commentstring=#\ %s

