# Plugins

The plugins are loaded using [vim-plug](https://github.com/junegunn/vim-plug).

```vim
call plug#begin('~/.vim/plugged')
```

## Appearance plugins

```vim
    Plug 'vim-airline/vim-airline'
    Plug 'gruvbox-community/gruvbox'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'junegunn/goyo.vim'
```

## Autocomplete and general language support

All language support and autocomplete is done using [coc.nvim](https://github.com/neoclide/coc.nvim)
the vimtex plugin is a requirement for the coc-vimtex plugin in coc.

```vim
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    Plug 'pixelneo/vim-python-docstring'
    Plug 'lervag/vimtex'
```

## Movement and integration plugins

Plugins to make moving around in vim faster and easier.

```vim
    Plug 'tpope/vim-vinegar'
    Plug 'preservim/nerdtree'
    Plug 'junegunn/vim-peekaboo'
    Plug 'unblevable/quick-scope'
    Plug 'justinmk/vim-sneak'
    Plug 'airblade/vim-gitgutter'
    Plug 'junegunn/fzf.vim'
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-surround'
    Plug 'jiangmiao/auto-pairs'
    Plug 'christoomey/vim-tmux-navigator'
```

## Markdown plugins

Specific plugins for markdown.

```vim
    Plug 'junegunn/vim-easy-align'
    Plug 'plasticboy/vim-markdown'
    Plug 'jkramer/vim-checkbox'
    Plug 'dkarter/bullets.vim'
    Plug 'chmp/mdnav'
    Plug 'dhruvasagar/vim-table-mode' 
call plug#end()
```
