# Init.vim

## Settings for neovim

### Sourcing general settings

| Include                           | Explanation                                                                   |
| -------                           | -----------                                                                   |
| [Plugins](vim/plugins.vim.md)     | Loads all the configs using [vim-plug](https://github.com/junegunn/vim-plug). |
| [Mappings](vim/maps.vim.md)       | Create all the keyboard mappings.                                             |
| [UI](vim/ui.vim.md)               | The appearance settings.                                                      |
| [Filetypes](vim/filetypes.vim.md) | Special filetype specific settings.                                           |
| [Commands](vim/commands.vim.md)   | Custom commands.                                                              |

```vim
source ~/.config/nvim/vim/plugins.vim
source ~/.config/nvim/vim/maps.vim
source ~/.config/nvim/vim/ui.vim
source ~/.config/nvim/vim/filetypes.vim
source ~/.config/nvim/vim/commands.vim
```

### Sourcing plugin specific settings

| Include                                 | Explanation                                                                                                              |
| -------                                 | -----------                                                                                                              |
| [coc](vim/plugins/coc.vim.md)           | Settings for [coc.nvim](https://github.com/neoclide/coc.nvim), most of the settings are straight from their github page. |
| [fzf](vim/plugins/fzf.vim.md)           | Settings for fzf.                                                                                                        |
| [vimtex](vim/plugins/vimtex.vim.md)     | Settings for vimtex.                                                                                                     |
| [checkbox](vim/plugins/checkbox.vim.md) | Settings for vim-checkbox.                                                                                               |

```vim
source ~/.config/nvim/vim/plugins/coc.vim
source ~/.config/nvim/vim/plugins/fzf.vim
source ~/.config/nvim/vim/plugins/vimtex.vim
source ~/.config/nvim/vim/plugins/checkbox.vim
```
