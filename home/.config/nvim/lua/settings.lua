local o = vim.o
local wo = vim.wo
local bo = vim.bo
local cmd = vim.cmd

-- global options
o.swapfile = true
o.dir = '/tmp'
o.smartcase = true
o.laststatus = 2
o.hlsearch = true
o.incsearch = true
o.ignorecase = true
o.scrolloff = 25
o.showmode = true
-- o.nofoldenable = true
-- o.colorscheme = 'gruvbox'

-- window-local options
wo.number = true
wo.wrap = true

-- buffer-local options
bo.expandtab = true
--
vim.g.airline_theme = "gruvbox"
vim.g.airline_extensions_tabline_enabled = 1
-- let g:airline_powerline_fonts = 0

o.background = "dark" -- or "light" for light mode
cmd([[colorscheme gruvbox]])
-- cmd([[AirlineTheme gruvbox]])
-- syntax on
-- filetype plugin indent on
-- highlight Pmenu ctermbg=darkgray guibg=black
-- hi Normal guibg=NONE ctermbg=NONE
-- set guifont=Hack:h14
-- let g:pandoc#modules#disabled = ["folding"]
-- " set autochdir
-- set clipboard+=unnamedplus " using system clipboard for vim
-- set nocompatible
-- set number relativenumber
-- set cursorline
-- set tabstop=4
-- set softtabstop=4   " number of spaces in tab when editing
-- set shiftwidth=4    " number of spaces to use for autoindent
-- set tabstop=4 shiftwidth=4 expandtab
-- set shell=bash\ 
-- set spelllang=en
-- let g:colorizer_auto_color = 1
-- set mouse=a
-- set nofoldenable    " disable folding
-- set conceallevel=0
