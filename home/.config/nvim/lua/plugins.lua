M = {}
local api = vim.api

-- Check if the packer tool exists
local packer_exists = pcall(vim.cmd, [[packadd packer.nvim]])

if not packer_exists then
  -- TODO: Maybe handle windows better?
  if vim.fn.input("Download Packer? (y for yes)") ~= "y" then
    return
  end

  local directory = string.format(
  '%s/pack/packer/opt',
  vim.fn.stdpath('config')
  )

  vim.fn.mkdir(directory, 'p')

  local out = vim.fn.system(string.format(
  'git clone %s %s',
  'https://github.com/wbthomason/packer.nvim',
  directory .. '/packer.nvim'
  ))

  print(out)
  print("Downloading packer.nvim...")

  return
end

return require('packer').startup(function()
    -- Packer can manage itself as an optional plugin
    use {'wbthomason/packer.nvim', opt = true}
    -- Appearance plugins
    use{ 'vim-airline/vim-airline'}
    use{ 'gruvbox-community/gruvbox'}
    use{ 'vim-airline/vim-airline-themes'}
    use{ 'junegunn/goyo.vim'}
    -- autocomplete and general language support
    use{ 'rhysd/vim-grammarous' }
    use{ 'neoclide/coc.nvim'}
    use{ 'pixelneo/vim-python-docstring'}
    use{ 'lervag/vimtex'}
    use{ 'matze/vim-tex-fold'}
    -- movement and integration plugins
    use{ 'vim-scripts/TaskList.vim' }
    use{ 'tpope/vim-vinegar'}
    use{ 'junegunn/vim-peekaboo'}
    use{ 'unblevable/quick-scope'}
    use{ 'justinmk/vim-sneak'}
    use{ 'airblade/vim-gitgutter'}
    use{ 'junegunn/fzf.vim'}
    use{ 'tpope/vim-commentary'}
    use{ 'tpope/vim-fugitive'}
    use{ 'tpope/vim-surround'}
    use{ 'jiangmiao/auto-pairs'}
    use{ 'christoomey/vim-tmux-navigator'}
    -- Markdown plugins
    use{ 'junegunn/vim-easy-align'}
    use{ 'plasticboy/vim-markdown'}
    use{ 'dhruvasagar/vim-marp'	}
    use{ 'jkramer/vim-checkbox'}
    use{ 'dkarter/bullets.vim'}
    use{ 'chmp/mdnav'}
    use{ 'dhruvasagar/vim-table-mode' }
end)

