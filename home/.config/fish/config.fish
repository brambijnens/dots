# Friendly shell (fish) configuration
## setting shell variables
export EDITOR=nvim
export BROWSER=firefox
export TERM="xterm-256color"

set PATH $HOME/.cargo/bin $PATH
set PATH $HOME/.local/bin $PATH
## shell variables for cluster
export vsc="vsc34039@login.hpc.kuleuven.be"
export data="/data/leuven/340/vsc34039/"

## sourcing aliasses
source "$HOME/.config/fish/aliases.cfg"

## fuctions
function latexreport
    cp -rf ~/Templates/latex/report $argv
end
function latexpresentation
    cp -rf ~/Templates/latex/presentation $argv
end

## disabling bell sound
set b off
set b 0 0 0

## appearance configuration
starship init fish | source
fish_vi_key_bindings

pfetch
